﻿using MonitorSettings.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorSettings.Helper
{
    public static class Util
    {
       public static T ReadAppKey<T>(string key)
        {
            T retValue = default(T);

            if ((System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(key)))
            {
                retValue = (T)Convert.ChangeType(ConfigurationManager.AppSettings[key], typeof(T));
            }

            return retValue;
        }
    }
}
