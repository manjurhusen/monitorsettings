﻿using MonitorSettings.Core.Interfaces;
using MonitorSettings.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TNM.NET45.Logging;

namespace MonitorSettings.Core
{
    public class Checks : IChecks
    {
        public CheckResult CheckWebServiceBaseUrl(string configValue, string dbValue)
        {
            CheckResult checkResult = new CheckResult();
            if (!String.IsNullOrEmpty(dbValue) && !String.IsNullOrEmpty(configValue)) 
            {
                if(configValue.Equals(dbValue,StringComparison.OrdinalIgnoreCase))
                {
                     checkResult.Status = 1;
                     checkResult.Message = string.Format("WebServiceBaseUrl Matched : Config Value: {0} , DataBase Value : {1}", configValue, dbValue);                    
                }
                else
                {
                    checkResult.Status = 0;
                    checkResult.Message = string.Format("WebServiceBaseUrl Not Matched : Config Value: {0} , DataBase Value : {1}", configValue, dbValue);                    
                }
            }
            else
            {
                checkResult.Status = 0;
                checkResult.Message = string.Format("WebServiceBaseUrl Not Found In Database or Config File.  Config Value: {0} , DataBase Value : {1}", configValue, dbValue);                
            }

            return checkResult;
        }
        
    }
}
