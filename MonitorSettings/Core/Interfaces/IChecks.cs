﻿using MonitorSettings.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorSettings.Core.Interfaces
{
    public interface IChecks
    {
        CheckResult CheckWebServiceBaseUrl(string configValue, string dbValue);
    }
}
