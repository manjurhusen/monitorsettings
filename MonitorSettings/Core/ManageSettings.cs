﻿using MonitorSettings.Configuration;
using MonitorSettings.Configuration.Interfaces;
using MonitorSettings.Core.Interfaces;
using MonitorSettings.Helper;
using MonitorSettings.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TNM.NET45.Logging;

namespace MonitorSettings.Core
{
    public class ManageSettings :IManageSettings
    {
        private readonly IChecks _Checks;
        public ManageSettings(IChecks Checks)
        {
            _Checks = Checks;
        }
               
        public void Monitor()
        {
           
            if (MonitorSettingsApp.SiteRootPaths != null && MonitorSettingsApp.SiteRootPaths.Count > 0)
            {
                TNMLogger.Instance.Log(string.Format("Info ::  SiteRootPaths Count: {0}", MonitorSettingsApp.SiteRootPaths.Count), TNMLogger.LoggerLevelSupported.Info);

                foreach (string root in MonitorSettingsApp.SiteRootPaths)
                {
                    TNMLogger.Instance.Log(string.Format("Info ::  SiteRootPath : {0}", root), TNMLogger.LoggerLevelSupported.Info);
                              
                    if (Directory.Exists(root))
                    {                           
                        IList<string> configRootPaths = new List<string>();
                        AddConfigRootPaths(root, configRootPaths);                        
                        
                        foreach (string configRootPath in configRootPaths)
                        {
                            TNMLogger.Instance.Log(string.Format("Info :: ConfigRootPath: {0}", configRootPath), TNMLogger.LoggerLevelSupported.Info);
                            try
                            {
                                var filePath = string.Empty;
                                var configType = ConfigType.WebConfig;
                                if (File.Exists(Path.Combine(configRootPath, "Mc.config"))) { filePath = Path.Combine(configRootPath, "Mc.config"); configType = ConfigType.McConfig; }
                                else if (File.Exists(Path.Combine(configRootPath, "Web.config"))) filePath = Path.Combine(configRootPath, "Web.config");

                                if (filePath != string.Empty)
                                {
                                    IXmlConfig xmlConfig = App.Container.Resolve<IXmlConfig>();
                                    xmlConfig.ParseXmlConfig(filePath, configType);
                                    if (configType == ConfigType.WebConfig && File.Exists(Path.Combine(configRootPath, "Environment.config")))
                                        xmlConfig.ParseXmlConfig(Path.Combine(configRootPath, "Environment.config"), ConfigType.EnvironmentConfig);

                                    if (!String.IsNullOrWhiteSpace(xmlConfig.MediaRoomShortName)) 
                                        CompareSettings(xmlConfig);
                                   //  Task.Factory.StartNew((d) => this.CompareSettings(xmlConfig), new CancellationToken());
                                    else TNMLogger.Instance.Log(string.Format("Info :: No MediaRoomShortName Fount, ConfigFilePath : {0}", filePath), TNMLogger.LoggerLevelSupported.Info);
                                }
                                else
                                {
                                    TNMLogger.Instance.Log(string.Format("Info :: No Config File Found. ConfigRootPath : {0}", configRootPath), TNMLogger.LoggerLevelSupported.Info);
                                }
                            }
                            catch (Exception ex)
                            {
                                TNMLogger.Instance.Log(ex.Message, TNMLogger.LoggerLevelSupported.Error);
                            }

                        }

                    }
                    else
                    {
                        TNMLogger.Instance.Log(string.Format("Info :: No Root Path Directory Found: {0}", root), TNMLogger.LoggerLevelSupported.Info);
                    }                                      

                }
            }
            else
            {
                TNMLogger.Instance.Log(string.Format("Info :: Monitor - SiteRootPaths Null or Empty - Value: {0}", MonitorSettingsApp.SiteRootPaths), TNMLogger.LoggerLevelSupported.Info);
            }
           
        }

        private  void AddConfigRootPaths(string path, IList<string> configRootPaths)
        {            
            try
            {
                //This will ignore case sensitivity
              //  Directory.EnumerateFiles(path, "Mc.config", SearchOption.TopDirectoryOnly)
                //            .Union(Directory.EnumerateFiles(path, "Web.config", SearchOption.TopDirectoryOnly)).Select(x => Path.GetDirectoryName(x.ToString().Trim())).Distinct().ToList().ForEach(s => configRootPaths.Add(s));

                //consider case sensitivity
                Directory.EnumerateFiles(path, "*.*", SearchOption.TopDirectoryOnly)
                   .Where(file => file.Trim().EndsWith("Web.config") || file.Trim().EndsWith("Mc.config"))
                   .Select(x => Path.GetDirectoryName(x.ToString().Trim())).Distinct().ToList().ForEach(s => configRootPaths.Add(s));
                
                Directory.GetDirectories(path)
                    .ToList()
                    .ForEach(s => AddConfigRootPaths(s, configRootPaths));
            }
            catch (UnauthorizedAccessException ex)
            {
                // ok, don't consider those directory which can't access
            }
          
        }

      
        private  void  CompareSettings(IXmlConfig xmlConfig)
        {
            IList<SettingItem> settings = MonitorSettingsApp.ServiceSettings.Where(x => x.Id == xmlConfig.MediaRoomShortName).ToList();

           //check for WebServiceUrl           
           CheckResult chkResult=_Checks.CheckWebServiceBaseUrl(xmlConfig.WebServiceBaseUrl, settings.Where(x => x.Key == "WebServiceBaseUrl").Select(y => y.Value).SingleOrDefault());
           if (chkResult.Status == 0) TNMLogger.Instance.Log(string.Format("Error :: ConfigFilePath:{0}, MediaRoomShortName:{1},  {2}", xmlConfig.ConfigFilePath, xmlConfig.MediaRoomShortName, chkResult.Message), TNMLogger.LoggerLevelSupported.Error);
           else TNMLogger.Instance.Log(string.Format("Info :: ConfigFilePath:{0}, MediaRoomShortName:{1},  {2}", xmlConfig.ConfigFilePath, xmlConfig.MediaRoomShortName, chkResult.Message), TNMLogger.LoggerLevelSupported.Info);
         
            //Add other checks here
        }

    }
}
