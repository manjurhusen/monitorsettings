﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorSettings.Model
{
    public class CheckResult
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
