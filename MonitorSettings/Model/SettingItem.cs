﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorSettings.Model
{
    public class SettingItem 
    {
        public string Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }        
    }
}
