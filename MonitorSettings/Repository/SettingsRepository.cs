﻿using MonitorSettings.Model;
using MonitorSettings.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TNM.NET45.DataAccess;

namespace MonitorSettings.Repository
{
   public class SettingsRepository : ISettingsRepository
    {
       public IList<SettingItem> GetServiceSettings()
       {
           return DataProvider<SettingItem>.GetInstance().Get("Proc_admin_GetMCServiceInformation");           
       }
    }
}
