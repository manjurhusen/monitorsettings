﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using MonitorSettings.Configuration;
using MonitorSettings.Configuration.Interfaces;
using MonitorSettings.Core;
using MonitorSettings.Core.Interfaces;
using MonitorSettings.Helper;
using MonitorSettings.Model;
using MonitorSettings.Repository;
using MonitorSettings.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorSettings
{
    static class App
    {
        private readonly static IWindsorContainer container = null;
        private readonly static string SiteRootPaths = "SiteRootPaths";

        static App()
        {
            container = new WindsorContainer().Install(FromAssembly.This());
        }
        public static void Prepare()
        {
            container.Register(Component.For<IChecks>().ImplementedBy<Checks>().LifestyleTransient());
            container.Register(Component.For<ISettingsRepository>().ImplementedBy<SettingsRepository>().LifestyleTransient());
            container.Register(Component.For<IManageSettings>().ImplementedBy<ManageSettings>().LifestyleTransient());
            container.Register(Component.For<IXmlConfig>().ImplementedBy<XmlConfig>().LifestyleTransient());

            var ConfigFilePathsValue = Util.ReadAppKey<string>(SiteRootPaths);
            if (!String.IsNullOrWhiteSpace(ConfigFilePathsValue))
            {
                MonitorSettingsApp.SiteRootPaths = ConfigFilePathsValue.Trim().Split(',').ToList();

                var repository = container.Resolve<ISettingsRepository>();
                MonitorSettingsApp.ServiceSettings = repository.GetServiceSettings();
            }
        }
        public static IWindsorContainer Container
        {
            get { return container; }
        }
    }

    internal static class MonitorSettingsApp
    {        
        public static IList<SettingItem> ServiceSettings { get; set; }
        public static IList<string> SiteRootPaths { get; set; }
    }
}
