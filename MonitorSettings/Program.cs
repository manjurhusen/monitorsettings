﻿using MonitorSettings.Core;
using MonitorSettings.Core.Interfaces;
using MonitorSettings.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TNM.NET45.Logging;

namespace MonitorSettings
{
     
    class Program
    {
        
        static void Main(string[] args)
        {
            App.Prepare();
            var manageSettings = App.Container.Resolve<IManageSettings>();            
            manageSettings.Monitor();   
            
        }
    }
}
