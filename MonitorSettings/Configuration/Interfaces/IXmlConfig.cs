﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MonitorSettings.Configuration.Interfaces
{
    public interface IXmlConfig
    {
        string MediaRoomShortName { get; }
        string MediaRoomName { get; }
        string WebServiceBaseUrl { get; }       
        string ConfigFilePath { get; }        
        void ParseXmlConfig(string path, ConfigType configType);
    }
}
