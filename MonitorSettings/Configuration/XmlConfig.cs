﻿using MonitorSettings.Configuration.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TNM.NET45.Logging;

namespace MonitorSettings.Configuration
{
    public class XmlConfig :IXmlConfig
    {
              
         public void ParseXmlConfig(string path, ConfigType configType)
         {
             try
             {
                 var document = XDocument.Load(path);
                 if (ConfigFilePath != null) ConfigFilePath = ConfigFilePath + ", " + path;
                 else ConfigFilePath = path;
                 Parse(document, configType);
             }
             catch (Exception ex)
             {
                 TNMLogger.Instance.Log(ex.Message, TNMLogger.LoggerLevelSupported.Error);
             }
           
         }
         private void Parse(XDocument document, ConfigType configType)
         {
             if (configType == ConfigType.McConfig)
             {
                 MediaRoomShortName = GetParameterValue(document, "MediaRoomShortName") ?? MediaRoomShortName;
                 MediaRoomName = GetParameterValue(document, "MediaRoomName") ?? MediaRoomName;
                 WebServiceBaseUrl = GetParameterValue(document, "WebServiceBaseUrl") ?? WebServiceBaseUrl;

             }
             else
             {
                 MediaRoomShortName = GetAppSettingValue(document, "MrShortName") ?? MediaRoomShortName;
                 MediaRoomName = GetAppSettingValue(document, "MCName") ?? MediaRoomName;
                 WebServiceBaseUrl = GetAppSettingValue(document, "ServiceBaseUrl") ?? WebServiceBaseUrl;                          
             }

             if (!String.IsNullOrEmpty(WebServiceBaseUrl)) WebServiceBaseUrl = WebServiceBaseUrl.TrimEnd('/');
         }

         public string MediaRoomShortName { get; private set; }
         public string MediaRoomName { get; private set; }        
         public string WebServiceBaseUrl { get; private set; }
         public string ConfigFilePath { get; private set; }

         protected static string GetParameterValue(XDocument document, string key)
         {
             var parameter = document.Root
                 .Element("Parameters")
                 .Elements("Add")
                 .FirstOrDefault(x => x.Attribute("Key").Value == key);

             if (parameter == null) return null;             
             return parameter.Attribute("Value").Value;
         }

         protected static string GetAppSettingValue(XDocument document, string key)
         {
             //var parameter = document.Root
             //    .Element("appSettings")
             //    .Elements("add")
             //    .FirstOrDefault(x => x.Attribute("key").Value == key);
             var parameter = document.Descendants("appSettings")                
                 .Elements("add")
                 .FirstOrDefault(x => x.Attribute("key").Value == key);            

             if (parameter == null)    return null;

             return parameter.Attribute("value").Value;
         }

      
    }
}
